/*! Speed gauge application */
use crate as apps;
use crate::Display;
use arrayvec::ArrayString;
use core::fmt::{Debug, Write};
use jore::div_round;
use ray_graphics::{Fixed, Monochrome, PixelStore, TiledRenderer};
use ray_graphics::dround as digits;
use ray_graphics::egc::{
    geometry::Dimensions,
    prelude::Point,
};
use ray_graphics::sdf;
use sentencer::CopySentencer;
use yanp;
use yanp::errors::NmeaSentenceError;
use yanp::parse::SentenceData;


fn to_dark(v: bool) -> Monochrome {
    if v { Monochrome::Dark }
    else { Monochrome::Light }
}

fn into_scene(f: impl Fn(Point) -> bool)
    -> ray_graphics::F<Monochrome, impl Fn(Point) -> Monochrome>
{
    ray_graphics::F(move |p: Point| to_dark(f(p)))
}

/// Angle precomputed for rotation
#[derive(Copy, Clone)]
struct Angle {
    sin: Fixed,
    cos: Fixed,
}

impl Angle {
    const fn from_radians(r: Fixed) -> Self {
        Self {
            sin: r.sin(),
            cos: r.cos(),
        }
    }
    
    const fn rotate(self, point: Point) -> Point {
        let Angle {sin, cos} = self;
        Point {
            x: cos.mul(point.x).add(sin.mul(point.y)).round(),
            y: cos.mul(point.y).sub(sin.mul(point.x)).round(),
        }
    }
}

/// Arc and two planes
fn arc_gauge(point: Point, angle: Angle) -> bool {
    let Point { x, y } = point;
    let circle = x*x + y*y < 176 * 176 / 4;
    let ring = circle && x*x + y*y > 100 * 100 / 4;
    let top = y <= 0;
    // Use angle instead of a precomputed tangent because tangent can divide by zero. This is more expensive though, at 4 multiplications instead of 1.
    let rplane = angle.rotate(point);
    let p = rplane.y > 0;
    top && ring && p
}

macro_rules! try_line {
    ($e:expr, $i:ident) =>{
        $e.map_err(|_| Error {line: line!(), e: ErrorKind::$i})?
    };
}

#[derive(Debug)]
pub enum ErrorKind {
    Sentencer,
    Format,
    Display,
    Render,
}

#[derive(Debug)]
pub struct Error {
    pub line: u32,
    pub e: ErrorKind,
}

pub fn run<
    E: Debug,
    C: apps::Console,
    G: apps::Gnss<Error=E>,
    D: Display<Error=E> + Dimensions + PixelStore<Monochrome> + TiledRenderer<Color=Monochrome, Error=core::convert::Infallible>
>(console: C, mut gnss: G, mut display: D) -> Result<(), Error>
    where <D as TiledRenderer>::Error: Debug
{
    let mut gbuf = [0; 64];

    let mut sentencer = CopySentencer::new();

    // yanp = "0.1.1" is a better API but needs manual piecing together
    let mut time = None;
    
    {
        display.set_area(display.bounding_box(), Monochrome::Light);
        try_line!(display.flush(), Display);
    }
    
    loop {
        let (read_count, ret) = gnss.read(&mut gbuf);
        try_line!(ret, Display);
        let raw = &gbuf[..read_count as usize];

        let mut outdated = false;

        try_line!(sentencer.add(raw), Sentencer);
        for s in try_line!(sentencer.read(), Sentencer) {
            let result = yanp::parse_nmea_sentence(s);

            fn cw<T: Debug>(v: Option<T>, s: &'static str) {
                v.map(|v| {
                    //writeln!(Console::writer(), "{} {:?}", s, v).unwrap();
                });
            }
            
            match result {
                // Ignore. This *will* happen, no reason to make it worse by stalling.
                Err(NmeaSentenceError::ChecksumError(_, _)) => {}
                Err(_e) => {
                    //writeln!(Console::writer(), "ERR {:?} {:?}", e, core::str::from_utf8(s)).unwrap();
                },
                /*
                // sat visibility
                Ok(SentenceData::GSV(s)) => {
                },
                // date time
                Ok(SentenceData::ZDA(_)) => {
                },
                */
                Ok(SentenceData::RMC(s)) => {
                    let mut time_sdf = None;
                    let mut speed_sdf = None;
                    if s.time != time {
                        time = s.time;
                        if let Some(t) = time {
                            let mut digits = ArrayString::<2>::new();
                            try_line!(write!(digits, "{:02}", t.second), Format);

                            time_sdf = Some(move |point| {
                                let point = sdf::point::translate(
                                    point,
                                    Point { x: 140, y: 150},
                                );
                                let point = sdf::point::scale(
                                    point,
                                    Fixed::one().mul(64).div(26),
                                );
                                digits::st(point, &digits[..])
                            });
                        }

                        outdated = true;
                    }
                    
                    if let Some((s, f)) = s.speed_knots {
                        let speed_knots_hundredths = s as u32 * 100 + f as u32;
                        let speed_dmh = speed_knots_hundredths * 1852 / 1000;
                        let speed_dmh = 1500;
                        let speed_kmh = div_round(speed_dmh, 100);
                        //writeln!(Console::writer(), "kmh {} dmh {}", speed_kmh, speed_dmh).unwrap();
                        let mut digits = ArrayString::<2>::new();
                        try_line!(write!(digits, "{:02}", speed_kmh), Format);

                        let speed_angle = Fixed::one()
                            .mul(speed_dmh as i32)
                            // mix to avoid overflow
                            .div(10000) // from 100 km/h full circle
                            .mulf(Fixed::TAU); // into full circle radians
                        let speed_angle = Angle::from_radians(speed_angle);
                        speed_sdf = Some(move |point| {
                            let digits = {
                                let point = sdf::point::translate(
                                    point,
                                    Point { x: 176/2 - 42, y: 176 - 112},
                                );
                                digits::st(point, &digits[..])
                            };
                            let point = sdf::point::translate(
                                point,
                                Point { x: 176 / 2, y: 176 / 2},
                            );
                            let gauge = arc_gauge(
                                point,
                                speed_angle,
                            );
                            digits || gauge
                        });

                        outdated = true;
                    }
                    
                    cw(s.time, "time");
                    cw(s.speed_knots, "speed");
                    let whole_sdf = move |point| {
                        time_sdf.map(|s| s(point)).unwrap_or(false)
                        || speed_sdf.map(|s| s(point)).unwrap_or(false)
                    };
                    try_line!(display.fill(
                        &display.bounding_box(),
                        into_scene(whole_sdf),
                    ), Render);
                },
                /*
                // position
                Ok(SentenceData::GLL(s)) => {
                    cw(s.time, "gltime");
                },*/
                // ground speed
                /*
                Ok(SentenceData::VTG(s)) => {
                    cw(s.speed_kmh, "speedkmh");
                    if let Some((s, _f)) = s.speed_kmh {
                        let mut digits = ArrayString::<2>::new();
                        write!(digits, "{}", s).unwrap();
                        Text::new(
                            &digits[..],
                            Point::new(5, 170),
                            text_style,
                        ).draw(&mut display)?;
        
                        outdated = true;
                    }
                },*/
                /*
                // active satellites
                Ok(SentenceData::GSA(_)) => {},
                // position again. This has more info
                Ok(SentenceData::GGA(_)) => {},
                // antenna open. yeah, whatever
                Ok(SentenceData::TXT(_)) => {},
                */
                Ok(_other) => {
                    //writeln!(Console::writer(), "Unhandled {:?}", other).unwrap();
                }
            }
        }
        if outdated {
            try_line!(display.flush(), Display);
        }
    }
}
