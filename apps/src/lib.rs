#![cfg_attr(not(feature="std"), no_std)]
pub mod geo;
pub mod map;
pub mod speed;

use core::fmt;

pub trait Gnss {
    type Error;
    fn read(&mut self, buf: &mut [u8]) -> (usize, Result<(), Self::Error>);
}

pub trait Display {
    type Error;
    fn flush(&mut self) -> Result<(), Self::Error>;
}

pub trait Console {
    type Writer: fmt::Write;
    fn writer(&self) -> Self::Writer;
}