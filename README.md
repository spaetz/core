Jazda core
======

This is a repository containing all original code for Jazda. It's actually a workspace with multiple projects.

sim
----

Starting point for developing Jazda. This is a simulator for Jazda applications. It contains code to run Jazda apps on your laptop.

The two demos available need a NMEA file with a track.

To run the "map" demo:

```
cd sim
cargo run --bin map < my_track.nmea
```

core
-----

This contains the code to run Jazda apps on the SMA Q3 smart watch. Start here, in the toplevel directory, if you want to flash things:

```
just build_bin map
```

That'll produce a file target/thumbv7em-none-eabi/release/map.tab. This file can be flashed using `tockloader install path/to/map.tab --board sma_q3 --openocd`.

apps
------

This is the place where abstract apps are created. They are structured as libraries, in order to be easily placed either on sim and core.

A long-term plan is to reverse this, and include sim and core as libraries instead, and to split interesting parts of apps (map display, speed gauge) into libraries. The actual apps would turn into glue between the parts.

This part contains geographic logic.

There's nothing to build here except for tests.

ray-graphics
----------------

This is a graphics library used by Jazda. While in principle, embedded-graphics also works, this library aims to be more of an exploration of the problem space (fragment shader-like rendering: can we support displays that expect tiles of many pixels at once? can a scene graph be as fast as immediate rendering? is the extra flexibility worth the speed penalty?)

It contains:
- drawing digits
- const fixed point operations

libtock-graphics
--------------------

This is glue code for supporting embedded-graphics with Tock. Currently unused in Jazda, may not work.
