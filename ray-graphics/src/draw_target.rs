use embedded_graphics_core::{
    geometry::Dimensions,
    pixelcolor::PixelColor,
    primitives::Rectangle,
};

use crate::Scene;

/// A device that's able to receive pixels in groups,
/// but not necessarily one by one.
///
/// The device decides which pixels to render. Because of that, it chooses how big of a buffer it needs. The buffer may cover 1 pixel at a time, or the entire display.
pub trait TiledRenderer: Dimensions {
    /// The pixel color type the targetted display supports.
    type Color: PixelColor;

    /// Error type to return when a drawing operation fails.
    ///
    /// This error is returned if an error occurred during a drawing operation. This mainly applies
    /// to drivers that need to communicate with the display for each drawing operation, where a
    /// communication error can occur. For drivers that use an internal framebuffer where drawing
    /// operations can never fail, [`core::convert::Infallible`] can instead be used as the `Error`
    /// type.
    ///
    /// [`core::convert::Infallible`]: https://doc.rust-lang.org/stable/core/convert/enum.Infallible.html
    type Error;
    
    /// Fill a given area with pixels coming from the scene.
    ///
    /// Use this method to update an area with colors from the given scene.
    /// The `area` is the minimal required update.
    /// The area actually updated may cover the entire display area,
    /// so the scene must be defined everywhere.
    ///
    /// The renderer is not aware of the scene contents, which means that specialized drawing procedures are not possible here.
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error>;
}
