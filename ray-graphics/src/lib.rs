#![no_std]

pub mod constrig;
pub mod digits;
pub mod dround;
pub mod draw_target;
pub mod painters;
pub mod sdf;
pub mod simulator;

pub use constrig::Fixed;
use core::iter;
use core::ops;
pub use embedded_graphics_core as egc;
use embedded_graphics_core::geometry::{Dimensions, Point};
use embedded_graphics_core::pixelcolor::PixelColor;

use embedded_graphics::primitives;

use embedded_graphics_core::primitives::Rectangle;

use embedded_graphics_core::prelude::{OriginDimensions, PointsIter, Size};

pub use draw_target::TiledRenderer;
use painters::Painter;


pub trait DivCeil<Rhs = Self> {
    type Output;
    fn div_ceil(self, rhs: Rhs) -> Self::Output;
}

/// Newer Rust introduces this natively,
/// but we don't always have newer Rust.
impl DivCeil for usize {
    type Output = Self;
    fn div_ceil(self, other: usize) -> Self::Output {
        let d = self / other;
        let m = self % other;
        if m == 0 { d } else { d + 1}
    }
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum Monochrome {
    Dark,
    Light,
}

impl PixelColor for Monochrome {
    type Raw = egc::pixelcolor::raw::RawU1;
}

pub trait Scene<Color> {
    // TODO: style instead of color?
    fn get_color(&self, point: Point) -> Color;
}

impl<C, T: Scene<C>> Scene<C> for &T {
    fn get_color(&self, point: Point) -> C {
        (*self).get_color(point)
    }
}

pub struct F<C, T: Fn(Point) -> C>(pub T);

impl<C, T: Fn(Point) -> C> Scene<C> for F<C, T> {
    fn get_color(&self, point: Point) -> C {
        self.0(point)
    }
}

#[derive(Clone)]
pub enum Primitive<'a> {
    Circle(primitives::Circle),
    Dark,
    Light,
    // Not optimal, wastes a lot of space for each element.
    // Still, best we have with a simple array
    // and with being able to copy (no lifetimes)
    //Text(Label),
    Sdf(&'a dyn Fn(Point) -> Monochrome),
    Scene(&'a dyn Scene<Monochrome>),
}

impl<'a> Scene<Monochrome> for Primitive<'a> {
    fn get_color(&self, point: Point) -> Monochrome {
        match self {
            Primitive::Circle(c) => c.get_color(point),
            Primitive::Dark => Dark.get_color(point),
            Primitive::Light => Monochrome::Light,
            Primitive::Sdf(f) => f(point),
            Primitive::Scene(s) => s.get_color(point),
            //Primitive::Text(t) => t.get_color(point),
        }
    }
}
/*
impl<'a> Scene<Monochrome> for SubImage<'a, ImageRaw<'_, BinaryColor>> {
    fn get_color(&self, point: Point) -> Monochrome {
        self.parent.get_color(self.area.top_left + point)
    }
}

// Really short labels only. Each pixel iterates characters.
#[derive(Clone)]
pub struct Label {
    pub font: MonoFont<'static>,
    pub text: ArrayString<8>,
}

impl Scene<Monochrome> for Label {
    fn get_color(&self, point: Point) -> Monochrome {
        /*if point.x > 1 || point.y > 4 {
            return Monochrome::Dark;
        }*/
        // TODO: bounds check
        let c = self.text.chars().enumerate()
            .find(|(i, _c)| {
                let left = (*i) as u32 * self.font.character_size.width;
                let right = left + self.font.character_size.width;
                point.x >= left as i32
                    && point.x < right as i32
                    && point.y >= 0
                    && point.y < self.font.character_size.height as i32
            });
        if let Some((i, c)) = c {
            let left = i as u32 * self.font.character_size.width;
            let img = self.font.glyph(c);
            img.get_color(Point {
                x: point.x - left as i32,
                y: point.y,
            })
        } else {
            Monochrome::Dark
        }
    }
}
*/
pub struct Dark;

impl Scene<Monochrome> for Dark {
    fn get_color(&self, _point: Point) -> Monochrome {
        Monochrome::Dark
    }
}

#[derive(Debug)]
/// A monochrome image
pub struct Image<const S: usize> {
    buf: [u8; S],
    width: usize,
    height: usize,
}

impl<const S: usize> Image<S> {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            buf: [0; S],
            width,
            height,
        }
    }
    fn pixel_index(&self, x: usize, y: usize) -> Option<(usize, u8)> {
        if x < self.width && y < self.height {
            let bytes_width = self.width.div_ceil(8);
            let byteidx = y * bytes_width + x / 8;
            let bitidx = 7 - x as u8 % 8;
            Some((byteidx, bitidx))
        } else {
            None
        }
    }
    pub fn get_buf(&self) -> &[u8] {
        &self.buf[..]
    }
}

/// Actually, it's just for memory buffers.
/// All this trait is for is to avoid specifying pixel packing.
pub trait PixelStore<T: Copy> {
    fn set(&mut self, x: usize, y: usize, value: T);
    fn get_size(&self) -> (usize, usize);
    fn set_area(&mut self, area: Rectangle, color: T) {
        for Point {x, y} in area.points() {
            self.set(x as usize, y as usize, color);
        }
    }
}

impl<const S: usize> PixelStore<Monochrome> for Image<S> {
    fn set(&mut self, x: usize, y: usize, value: Monochrome) {
        if let Some((byteidx, bitidx)) = self.pixel_index(x, y) {
            fn clr(byte: u8, bit: u8) -> u8 {
                byte & !bit
            }
            
            use ops::BitOr;
            let op = match value {
                Monochrome::Dark => clr,
                Monochrome::Light => u8::bitor,
            };
            self.buf[byteidx] = op(self.buf[byteidx], 1<< bitidx);
        }
    }
    
    fn get_size(&self) -> (usize, usize) { (self.width, self.height) }
}

impl<const S: usize> Scene<Option<Monochrome>> for Image<S> {
    fn get_color(&self, point: Point) -> Option<Monochrome> {
        if let Some((byteidx, bitidx))
            = self.pixel_index(point.x as usize, point.y as usize)
        {
            let byte = self.buf[byteidx];
            let bit = byte & (1 << bitidx);
            Some(
                if bit == 0 { Monochrome::Dark }
                else { Monochrome::Light }
            )
        } else {
            None
        }
    }
}

impl<const S: usize> Scene<Monochrome> for Image<S> {
    fn get_color(&self, point: Point) -> Monochrome {
        <Self as Scene<Option<Monochrome>>>::get_color(self, point)
            .unwrap_or(Monochrome::Dark)
    }
}


impl<const S: usize> OriginDimensions for Image<S> {
    fn size(&self) -> Size {
        let (width, height) = self.get_size();
        Size {width: width as u32, height: height as u32}
    }
}


impl<const B: usize> TiledRenderer for Image<B> {
    type Color = Monochrome;
    type Error = core::convert::Infallible;
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error> {
        for point in area.intersection(&self.bounding_box()).points() {
            self.set(point.x as usize, point.y as usize, scene.get_color(point));
        }
        Ok(())
    }
}


fn iter_rect(mut x: i32, mut y: i32, w: i32, h: i32) -> impl Iterator<Item=(i32, i32)> {
    iter::from_fn(move || {
        let ox = x;
        let oy = y;
        (x, y) = if x < w { (x + 1, y) } else { (0, y + 1) };
        if oy < h {
            Some((ox, oy))
        } else {
            None
        }
    })
}

pub struct TwoLevel<'a, P: Painter> {
    /// background won't be drawn where foreground is
    pub background: Primitive<'a>,
    pub foreground: (Rectangle, P),
}

impl<'a, P: Painter> TwoLevel<'a, P> {
    pub fn draw<I: PixelStore<Monochrome>>(&self, image: &mut I) {
        let bounds = self.foreground.0;
        image.set_area(bounds, Monochrome::Light);
        self.foreground.1.draw(image, Monochrome::Dark);
        let (width, height) = image.get_size();
        for (x, y) in iter_rect(0, 0, width as i32, height as i32) {
            let point = Point { x, y };
            if !bounds.contains(point) {
                image.set(
                    x as usize,
                    y as usize,
                    self.background.get_color(point)
                );
            }
        }
    }
}

/// ORs every pixel
pub struct Stack<'a>(pub [Primitive<'a>; 8]);

impl<'a> Scene<Monochrome> for Stack<'a> {
    fn get_color(&self, point: Point) -> Monochrome {
        if self.0.iter()
            .find(|p| p.get_color(point) == Monochrome::Dark)
            .is_some() { Monochrome::Dark }
        else { Monochrome::Light }
    }
}

/*
struct Arc {
}

impl Scene<Monochrome> for primitives::Circle {
    fn get_color(&self, point: Point) -> Monochrome {
        let r = 16;
        let r2 = 11;
        let dist = sqrt(dot(point, point));
        let sm = r<dist;
        let sm2 = r2>dist;
        let circle = sm&sm2;
        let g = point.x / dist;
        let sector = g*g/
        circle & sector;
    }
}   
*/

impl Scene<Monochrome> for primitives::Circle {
    fn get_color(&self, point: Point) -> Monochrome {
        let rsquare = self.diameter * self.diameter / 4;
        let a = self.center().x - point.x;
        let b = self.center().y - point.y;
        if rsquare >= (a * a) as u32 + (b * b) as u32 { Monochrome::Light }
        else { Monochrome::Dark }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn ugh() {
        let mut im = Image::<100>::new(10, 10);
        im.set(0, 1, Monochrome::Light);
        for (x, y) in iter_rect(0, 0, 10, 10) {
            if x != 0 && y != 1 {
                assert_eq!(
                    <Image<100> as Scene<Monochrome>>::get_color(
                        &im,
                        Point {x, y},
                    ),
                    Monochrome::Dark,
                );
            }
        }
    }
    
    #[test]
    fn indices() {
        let im = Image::<100>::new(6, 6);
        assert_eq!(im.pixel_index(0, 1), Some((1, 7)));
    }
}