/*! Drawing nice round digits implemented as SDFs
This is a rounder font based on URW Gothic.
It has only 2 circle sizes.

Hints: circles in integer calculations have their middles in the middle of pixels. So do mirrors, unless adjusted.
Not clear how to adjust circle.
That means circles will have nubs on sides, top, and bottom.
Also, aligning circles next to each other will cause them to occupy one pixel more than expected. This is evened out by making the small circle smaller.

Here, the workaround is to add a little squared radius. This makes the nubs bigger.

The centering on middle of pixels also causes clipping on the high sides because of making the size odd-numbered. In this case, the clipping on the bottom doesn't actually hurt.
*/

use crate::{Fixed, Monochrome};
use crate::egc::primitives::Rectangle;
use crate::egc::prelude::{Point, Size};


/// All digits clipped to this size
const DIGIT_SIZE: Rectangle = Rectangle {
    top_left: Point { x: 0, y: 0 },
    size: Size {width: 41, height: 65}
};


/// Circle with origin at 0
fn circle(point: Point, diameter: u32) -> bool {
    let rsquare = diameter * diameter / 4 + diameter;
    let a = point.x;
    let b = point.y;
    rsquare >= (a * a) as u32 + (b * b) as u32
}

fn scale(point: Point, xs: Fixed, ys: Fixed) -> Point {
    let Point { x, y } = point;
    Point {
        x: xs.mul(x).round(),
        y: ys.mul(y).round(),
    }
}


fn line(point: Point, angle: Point, thickness: u32) -> bool {
    let k = angle.y*point.x - angle.x*point.y;
    let f = (k*k / (angle.x*angle.x+angle.y*angle.y)) as u32;
    f < thickness
}

pub(crate) mod uncut {
    use super::*;
    
    
    fn bigcircle(point: Point) -> bool {
        // thickness should be 8, but somehow it's smaller when set exactly
        circle(point, 40) &! circle(point, 22)
    }
    
    
    fn smallcircle(point: Point) -> bool {
        // thickness should be 8, but somehow it's smaller when set exactly
        circle(point, 31) &! circle(point, 14)
    }
    
    fn botcircle(point: Point) -> bool {
        let point = point - Point { x: 20, y: 44 };
        bigcircle(point)
    }
    
    fn topcircle(point: Point) -> bool {
        let point = point - Point { x: 20, y: 16 };
        smallcircle(point)
    }
    
    /// mirrors lower values into higher values around a pivot *after* the given value.
    /// With pivot 4, 4 and 5 will be both turned into 5.
    fn mirror_even(x: i32, pivot_after: i32) -> i32 {
        if x < pivot_after { pivot_after * 2 - 1 - x }
        else { x }
    }
    
    pub fn zero(point: Point) -> bool {
        let Point {x, y} = point;
        // mirror the bottom-right quadrant into all 4
        let point = Point {
            x: mirror_even(x, 20),
            y: mirror_even(y, 32),
        };
        let ring = {
            botcircle(point) && point.y >= 44
        };
        ring || {
            // line
            point.x >= 32 && point.y < 44
        }
    }

    pub fn one(point: Point) -> bool {
        let Point {x, y} = point;
        // center it I guess? The major line will be somewhat to the right
        x >= 12 && x < 28
        && {
            // nose
            y < 8
            // major line
            || x > 20
        }
    }

    pub fn two(point: Point) -> bool {
        let circle_center = Point { x: 20, y: 20 };
        let cpoint = point - circle_center;
        let skew_plane = cpoint.x > cpoint.y;
        // horizontal
        point.y >= 56
        // slanted
        || {
            !skew_plane
            && line(point - Point { x: -4, y: 64 }, Point { x: 52, y: -48 }, 16)
        }
        // hook
        || {
            let point = cpoint;
            (point.y <= 0 || skew_plane) & bigcircle(point)
        }
    }

    pub fn three(point: Point) -> bool {
        let top = {
            topcircle(point) && (
                point.y <= 16 || point.x > 20
            )
        };
        top || {
            botcircle(point) && (
                point.y > 44 || point.x > 20
            )
        }
    }


    pub fn four(point: Point) -> bool {
        let Point {x, y} = point;
        let vertical = x > 24 && x <= 32;
        vertical
        || {
            y <= 48
            && {    
                line(point - Point { x: -4, y: 56 }, Point { x: 36, y: -64 }, 16)
                || y > 40
            }
        }
    }

    pub fn five(point: Point) -> bool {
        // horizontal
        let horz = {
            point.y <= 8 && point.x > 12 && point.x <= 36
        };
        // back
        horz || {
            line(point - Point { x: 8, y: 36 }, Point { x: 4, y: -36 }, 16)
            && point.y <= 36
        }
        // circle
        || {
            let point = point - Point { x: 20, y: 44 };
            bigcircle(point)
            && (
                point.y > 0
                || point.x > point.y
            )
        }
    }

    pub fn six(point: Point) -> bool {
        botcircle(point)
        || {
            line(point - Point { x: 0, y: 44 }, Point { x: 28, y: -48 }, 16)
            && point.y < 40
        }
    }

    pub fn seven(point: Point) -> bool {
        // horz
        point.y <= 8
        || line(point - Point { x: 4, y: 64 }, Point { x: 36, y: -64 }, 16)
    }

    pub fn eight(point: Point) -> bool {
        botcircle(point) || topcircle(point)
    }

    pub fn nine(point: Point) -> bool {
        let Point {x, y} = point;
        let point = Point {
            x: 40 - x,
            y: 64 - y,
        };
        six(point)
}
}

fn digit(point: Point, offset: Point, digit: impl Fn(Point) -> bool) -> bool {
    let point = point - offset;
    DIGIT_SIZE.contains(point) && digit(point)
}

macro_rules! to_digit {
    ($d:ident) => {
        pub fn $d(point: Point, offset: Point) -> bool {
            digit(point, offset, uncut::$d)
        }
    }
}

to_digit!(zero);
to_digit!(one);
to_digit!(two);
to_digit!(three);
to_digit!(four);
to_digit!(five);
to_digit!(six);
to_digit!(seven);
to_digit!(eight);
to_digit!(nine);

pub const DIGITS: [fn(Point, Point) -> bool; 10] = [
    zero, one, two, three, four, five, six, seven, eight, nine,
];

pub fn to_scene(digit: impl Fn(Point, Point) -> bool, offset: Point)
    -> impl Fn(Point) -> Monochrome
{
    move |point| {
        if !digit(point, offset) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}

pub fn st(point: Point, s: &str) -> bool {
    s.chars()
        .enumerate()
        .find(|(i, c)| match c.to_digit(10) {
            Some(d) => DIGITS[d as usize](
                point,
                Point { x: *i as i32 * 44, y: 0 },
            ),
            None => false,
        })
        .is_some()
}


pub fn n(point: Point, digits: &[u8]) -> bool {
    digits.iter()
        .enumerate()
        .rev()
        .find(|(i, d)| {
            if **d < 10 {
                DIGITS[**d as usize](
                    point,
                    Point { x: *i as i32 * 44, y: 0 },
                )
            } else {
                false
            }
        })
        .is_some()
}


pub fn string<'a>(s: &'a str) -> impl Fn(Point) -> Monochrome + 'a {
    move |point| {
        if st(point, s) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}

pub fn num<'a>(s: &'a[u8]) -> impl Fn(Point) -> Monochrome + 'a {
    move |point| {
        if n(point, s) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}