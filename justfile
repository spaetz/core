reqs:
    # does not actually work.
    cargo install --path ../libtock-rs/runner/ --bin runner

build:
    LIBTOCK_PLATFORM=nrf52840 cargo build --target=thumbv7em-none-eabi --release

build_bin NAME: build
    LIBTOCK_PLATFORM=nrf52840 elf2tab --kernel-major 2 --kernel-minor 0 -n {{NAME}} -o target/thumbv7em-none-eabi/release/{{NAME}}.tab target/thumbv7em-none-eabi/release/{{NAME}},cortex-m4
