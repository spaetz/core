use apps::Display;
use core::fmt::Debug;
use embedded_graphics_simulator::{SimulatorDisplay, Window, OutputSettingsBuilder};
use ray_graphics::{Fixed, Monochrome, PixelStore, Primitive, Scene, Stack, TiledRenderer};
use ray_graphics::dround as digits;
use sim;
use sim::{MonoTranslate};
use std::thread;
use std::time::Duration;
use ray_graphics::egc::{
    geometry::Dimensions,
    pixelcolor::BinaryColor,
    prelude::{Point, Size},
};

fn main() {
    let display = SimulatorDisplay::<BinaryColor>::new(Size::new(220, 176));
    let display = MonoTranslate{ disp: display };

    let output_settings = OutputSettingsBuilder::new()
        .scale(1)
        .build();
    let w = Window::new("Digits test", &output_settings);
    let display = sim::Display { w, buffer: display };
    run(display).unwrap()
}

struct Scale<T: Scene<Monochrome>>{
    scene: T,
    factor: Fixed,
}

impl <T: Scene<Monochrome>> Scene<Monochrome> for Scale<T> {
    fn get_color(&self, p: Point) -> Monochrome {
        let Point { x, y } = p;
        self.scene.get_color(Point {
            x: self.factor.mul(x).round(),
            y: self.factor.mul(y).round(),
        })
    }
}


pub fn run<E, D>(mut display: D) -> Result<(), E> 
where
    E: Debug,
    D: Display<Error=E>
        + Dimensions
        + PixelStore<Monochrome>
        + TiledRenderer<Color=Monochrome>,
    <D as TiledRenderer>::Error: Debug,
{
    // Draw numbers
    let mut stack = Stack([
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
    ]);
    let z = digits::to_scene(digits::zero, Point{ x: 0, y: 0 });
    stack.0[0] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::one, Point{ x: 45, y: 0 });
    stack.0[1] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::two, Point{ x: 90, y: 0 });
    stack.0[2] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::three, Point{ x: 135, y: 0 });
    stack.0[3] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::four, Point{ x: 180, y: 0 });
    stack.0[4] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::five, Point{ x: 0, y: 85 });
    stack.0[5] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::six, Point{ x: 45, y: 85 });
    stack.0[6] = Primitive::Sdf(&z);
    
    let mut stack2 = Stack([
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
        Primitive::Light,
    ]);
    
    let z = digits::to_scene(digits::seven, Point{ x: 90, y: 85 });
    stack2.0[0] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::eight, Point{ x: 135, y: 85 });
    stack2.0[1] = Primitive::Sdf(&z);
    let z = digits::to_scene(digits::nine, Point{ x: 180, y: 85 });
    stack2.0[2] = Primitive::Sdf(&z);
    
    
    stack.0[7] = Primitive::Scene(&stack2);
    
    
    loop {
        //for i in 1..80 {
        for i in 64..65 {
            let scene = Scale {
                scene: &stack,
                factor: Fixed::one().mul(64).div(i),
            };
            display.fill(&display.bounding_box(), scene).unwrap();
            display.flush()?;
            thread::sleep(Duration::from_millis(200));
        }
    }
}