use embedded_graphics_simulator::{SimulatorDisplay, Window, OutputSettingsBuilder};
use apps::map;
use sim;
use sim::{ErrorCode, MonoTranslate};
use ray_graphics::egc::{
    pixelcolor::BinaryColor,
    prelude::Size,
};

fn main() {
    m().unwrap();
}

fn m() -> Result<(), ErrorCode> {
    let gnss = sim::Gnss;

    let display = SimulatorDisplay::<BinaryColor>::new(Size::new(176, 176));
    let display = MonoTranslate{ disp: display };

    let output_settings = OutputSettingsBuilder::new()
        .scale(2)
        .build();
    let w = Window::new("Map test", &output_settings);
    let display = sim::Display { w, buffer: display };
    map::run(gnss, display)
}