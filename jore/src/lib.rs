#![no_std]

pub fn div_round(n: u32, d: u32) -> u32 {
    let a = n * 2 / d;
    a / 2 + (a & 1)
}

pub fn knots_to_kmh(whole: u16, hundredths: u8) -> u8 {
    let speed_knots_hundredths = whole as u32 * 100 + hundredths as u32;
    let speed_dmh = speed_knots_hundredths * 1852 / 1000;
    div_round(speed_dmh, 100) as u8
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(knots_to_kmh(10, 0), 19);
    }
}
